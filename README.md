# Coinbase Exercise

The solution to the exercise has been implemented in Elixir and the Phoenix framework.

## Starting the application

If you have Elixir installed on your system (see `.tool-versions` file for version) run:

```shell
MIX_ENV=prod mix deps.get
MIX_ENV=prod PORT=6000 mix phx.server
```

If you don't have Elixir installed you can run the packaged binary included in the repo:

```shell
PORT=7777 ./release/coinbase/bin/coinbase foreground
```

## Running the tests

```shell
mix test
```

Hint: To display the test case descriptions, use: `mix test --trace`.

You may check for compilation warnings using:

```shell
mix compile --warnings-as-errors
```

## Implementation Notes

Each orderbook is modeled as a separate process which encapsulates the
responsibility to refresh its data using the API and possibly support
other detail levels (eg streaming).

All function calls which can fail, return tagged tuples {:ok, val} | {:error, error}.

The repo commit history is not polished, I would rebase and squash
fixups, but this way it might give a more accurate idea of how it was developed.
