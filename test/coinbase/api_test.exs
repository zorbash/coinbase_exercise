defmodule Coinbase.APITest do
  use ExUnit.Case
  import ExUnit.CaptureLog

  alias Coinbase.API, as: Subject

  setup do
    config = Application.get_env :coinbase, Subject
    bypass = Bypass.open

    Application.put_env :coinbase, Subject, put_in(config[:base_url], base_url(bypass.port))
    on_exit fn -> Application.put_env :coinbase, Subject, config end

    {:ok, %{bypass: bypass}}
  end

  describe "products/0" do
    setup do
      {:ok, %{subject: &Subject.products/0}}
    end

    test "with an error response status, returns an error tuple with the status",
    %{subject: subject, bypass: bypass} do
      Bypass.expect_once bypass, "GET", "/products", fn conn ->
        Plug.Conn.resp(conn, 429, ~s<{"errors": [{"code": 88, "message": "Rate limit exceeded"}]}>)
      end

      capture_log fn ->
        assert {:error, "failed with status: 429"} = subject.()
      end
    end

    test "with an error response, returns an error tuple", %{subject: subject, bypass: bypass} do
      Bypass.down(bypass)

      capture_log fn ->
        assert {:error, "failed with message: adapter error: :econnrefused"} = subject.()
      end
    end

    test "with a successful response, returns a success tuple with the parsed JSON as a Map",
    %{subject: subject, bypass: bypass} do
      Bypass.expect_once bypass, "GET", "/products", fn conn ->
        conn
        |> Plug.Conn.put_resp_header("content-type", "application/json")
        |> Plug.Conn.resp(200, fixture(:products) |> Poison.encode!)
      end

      assert {:ok, products} = subject.()
      assert products == fixture(:products)
    end

    defp fixture(:products) do
      [
        %{
          id: "BCH-EUR",
          base_currency: "BCH",
          base_max_size: "120",
          base_min_size: "0.01",
          cancel_only: false,
          display_name: "BCH/EUR",
          limit_only: false,
          margin_enabled: false,
          max_market_funds: "200000",
          min_market_funds: "10",
          post_only: false,
          quote_currency: "EUR",
          quote_increment: "0.01",
          status: "online",
          status_message: nil
        }
      ]
    end
  end

  describe "book/2" do
    setup do
      {:ok, %{subject: fn -> Subject.book("ETH-EUR", %{level: 2}) end}}
    end

    test "with an error response status, returns an error tuple with the status",
    %{subject: subject, bypass: bypass} do
      Bypass.expect_once bypass, "GET", "/products/ETH-EUR/book", fn conn ->
        Plug.Conn.resp(conn, 429, ~s<{"errors": [{"code": 88, "message": "Rate limit exceeded"}]}>)
      end

      capture_log fn ->
        assert {:error, "failed with status: 429"} = subject.()
      end
    end

    test "with an error response, returns an error tuple", %{subject: subject, bypass: bypass} do
      Bypass.down(bypass)

      capture_log fn ->
        assert {:error, "failed with message: adapter error: :econnrefused"} = subject.()
      end
    end

    test "adds the level query parameter" , %{subject: subject, bypass: bypass} do
      Bypass.expect_once bypass, "GET", "/products/ETH-EUR/book", fn conn ->
        conn = conn
        |> Plug.Conn.fetch_query_params()
        |> Plug.Conn.resp(200, fixture(:book) |> Poison.encode!)

        assert %{params: %{"level" => "2"}} = conn

        conn
      end

      subject.()
    end

    test "with a successful response, returns a success tuple with the parsed JSON as a Map",
    %{subject: subject, bypass: bypass} do
      Bypass.expect_once bypass, "GET", "/products/ETH-EUR/book", fn conn ->
        conn
        |> Plug.Conn.put_resp_header("content-type", "application/json")
        |> Plug.Conn.resp(200, fixture(:book) |> Poison.encode!)
      end

      assert {:ok, book} = subject.()
      assert book == fixture(:book)
    end

    defp fixture(:book) do
      %{
        asks: [["6942", "0.43937637", 1]],
        bids: [["6695", "0.24682503", 1]],
        sequence: 2360345711
      }
    end
  end

  defp base_url(port), do: "http://localhost:#{port}"
end
