defmodule Coinbase.QuoteEngineTest do
  use ExUnit.Case, async: true

  alias Decimal, as: D
  alias Coinbase.QuoteEngine, as: Subject
  import Decimal, only: [to_float: 1]

  describe "quote/3 - buy" do
    test "with a zero amount, returns error" do
      assert {:error, :zero_amount} = %{asks: []} |> Subject.quote(:buy, %{amount: 0})
    end

    test "with a zero string amount, returns error" do
      assert {:error, :zero_amount} =  %{asks: []} |> Subject.quote(:buy, %{amount: "0"})
    end

    test "with a negative amount, returns error" do
      assert {:error, :negative_amount} = %{asks: []} |> Subject.quote(:buy, %{amount: -1})
    end

    test "with an negative string amount, returns error" do
      assert {:error, :negative_amount} = %{asks: []} |> Subject.quote(:buy, %{amount: "-1"})
    end

    test "when there are no :asks in the book" do
      assert {:error, :insufficient_orders} = %{asks: []} |> Subject.quote(:buy, %{amount: 1})
    end

    test "with a string amount covered by a single order, returns the correct price and total" do
      assert fixtures(:buy, :book_single_order)
             |> Subject.quote(:buy, %{amount: "1"}) == {:ok, %{price: D.new(666), total: D.new(666)}}
    end

    test "with an amount covered by a single order, returns the correct price and total" do
      assert fixtures(:buy, :book_single_order)
             |> Subject.quote(:buy, %{amount: 1}) == {:ok, %{price: D.new(666), total: D.new(666)}}
    end

    test "with an amount covered by a single aggregated order, returns the correct price and total" do
      expected = {:ok, %{price: D.new(777), total:  D.new(777)}}

      assert fixtures(:buy, :book_single_aggregated_order) |> Subject.quote(:buy, %{amount: 1}) == expected
    end

    test "with an amount which cannot be covered by multiple orders, returns an error" do
      assert {:error, :insufficient_orders} = fixtures(:buy, :book_multiple_orders)
                                              |> Subject.quote(:buy, %{amount: 1_000_000})
    end

    test "with an amount which can be covered by multiple orders, returns the weighted price and total" do

      assert {:ok, %{price: price, total: total}} = fixtures(:buy, :book_multiple_orders)
                                                    |> Subject.quote(:buy, %{amount: 1.4})
      assert %{price: 228.5714285714285714285714286, total: 320.0} = %{price: to_float(price), total: to_float(total)}
    end
  end

  describe "quote/3 - buy - inverted" do
    test "with a zero amount, returns error" do
      assert {:error, :zero_amount} = %{bids: []} |> Subject.quote(:buy, %{amount: 0, inverted?: true})
    end

    test "with a zero string amount, returns error" do
      assert {:error, :zero_amount} =  %{bids: []} |> Subject.quote(:buy, %{amount: "0", inverted?: true})
    end

    test "with a negative amount, returns error" do
      assert {:error, :negative_amount} = %{bids: []} |> Subject.quote(:buy, %{amount: -1, inverted?: true})
    end

    test "with an negative string amount, returns error" do
      assert {:error, :negative_amount} = %{bids: []} |> Subject.quote(:buy, %{amount: "-1", inverted?: true})
    end

    test "when there are no :bids in the book" do
      assert {:error, :insufficient_orders} = %{bids: []} |> Subject.quote(:buy, %{amount: 1, inverted?: true})
    end

    test "with a string amount covered by a single order, returns the correct price and total" do
      {:ok, %{price: price, total: total}} = fixtures(:sell, :book_single_order)
                                            |> Subject.quote(:buy, %{amount: "200", inverted?: true})
      assert {D.to_float(price), D.to_float(total)} == {0.0015015015015015015, 0.3003003003003003}
    end

    test "with an amount covered by a single order, returns the correct price and total" do
      {:ok, %{price: price, total: total}} = fixtures(:sell, :book_single_order)
                                            |> Subject.quote(:buy, %{amount: 200, inverted?: true})

      assert {to_float(price), to_float(total)} == {0.0015015015015015015, 0.3003003003003003}
    end

    test "with an amount which cannot be covered by multiple orders, returns an error" do
      assert {:error, :insufficient_orders} = fixtures(:sell, :book_multiple_orders)
                                              |> Subject.quote(:buy, %{amount: 1_000_000, inverted?: true})
    end

    test "with an amount which can be covered by multiple orders, returns the weighted price and total" do
      assert {:ok, %{price: price, total: total}} = fixtures(:sell, :book_multiple_orders)
                                                    |> Subject.quote(:buy, %{amount: 200, inverted?: true})

      assert %{price: 0.50475, total: 100.95} = %{price: to_float(price), total: to_float(total)}
    end
  end

  describe "quote/3 - sell" do
    test "with a zero amount, returns error" do
      assert {:error, :zero_amount} = %{bids: []} |> Subject.quote(:sell, %{amount: 0})
    end

    test "with a zero string amount, returns error" do
      assert {:error, :zero_amount} = %{bids: []} |> Subject.quote(:sell, %{amount: "0"})
    end

    test "with a negative amount, returns error" do
      assert {:error, :negative_amount} = %{bids: []} |> Subject.quote(:sell, %{amount: -1})
    end

    test "with a negative string amount, returns error" do
      assert {:error, :negative_amount} = %{bids: []} |> Subject.quote(:sell, %{amount: "-1"})
    end

    test "when there are no :asks in the book" do
      assert {:error, :insufficient_orders} = %{bids: []} |> Subject.quote(:sell, %{amount: 1})
    end

    test "with a string amount covered by a single order, returns the correct price and total" do
      assert fixtures(:sell, :book_single_order)
             |> Subject.quote(:sell, %{amount: "1"}) == {:ok, %{price: D.new(666), total: D.new(666)}}
    end

    test "with an amount covered by a single order, returns the correct price and total" do
      assert fixtures(:sell, :book_single_order)
             |> Subject.quote(:sell, %{amount: 1}) == {:ok, %{price: D.new(666), total: D.new(666)}}
    end

    test "with an amount covered by a single aggregated order, returns the correct price and total" do
      expected = {:ok, %{price: D.new(777), total:  D.new(777)}}

      assert fixtures(:sell, :book_single_aggregated_order) |> Subject.quote(:sell, %{amount: 1}) == expected
    end

    test "with an amount which cannot be covered by multiple orders, returns an error" do
      assert {:error, :insufficient_orders} = fixtures(:sell, :book_multiple_orders) |> Subject.quote(:sell, %{amount: 1_000_000})
    end

    test "with an amount which can be covered by multiple orders, returns the weighted price and total" do
      quote = fixtures(:sell, :book_multiple_orders) |> Subject.quote(:sell, %{amount: 1.4})

      assert {:ok, %{price: price, total: total}} = quote
      assert %{price: 228.5714285714285714285714286, total: 320.0} = %{price: to_float(price), total: to_float(total)}
    end
  end

  describe "quote/3 - sell - inverted" do
    test "with a zero amount, returns error" do
      assert {:error, :zero_amount} = %{asks: []} |> Subject.quote(:sell, %{amount: 0, inverted?: true})
    end

    test "with a zero string amount, returns error" do
      assert {:error, :zero_amount} = %{asks: []} |> Subject.quote(:sell, %{amount: "0", inverted?: true})
    end

    test "with a negative amount, returns error" do
      assert {:error, :negative_amount} = %{asks: []} |> Subject.quote(:sell, %{amount: -1, inverted?: true})
    end

    test "with a negative string amount, returns error" do
      assert {:error, :negative_amount} = %{asks: []} |> Subject.quote(:sell, %{amount: "-1", inverted?: true})
    end

    test "when there are no :asks in the book" do
      assert {:error, :insufficient_orders} = %{asks: []} |> Subject.quote(:sell, %{amount: 1, inverted?: true})
    end

    test "with a string amount covered by a single order, returns the correct price and total" do
      assert {:ok, %{price: price, total: total}} = fixtures(:buy, :book_single_order)
             |> Subject.quote(:sell, %{amount: "2", inverted?: true})
      assert {to_float(price), to_float(total)} == {0.0015015015015015015, 0.003003003003003003}
    end

    test "with an amount covered by a single order, returns the correct price and total" do
      assert {:ok, %{price: price, total: total}} = fixtures(:buy, :book_single_order)
             |> Subject.quote(:sell, %{amount: 2, inverted?: true})
      assert {to_float(price), to_float(total)} == {0.0015015015015015015, 0.003003003003003003}
    end

    test "with an amount covered by a single aggregated order, returns the correct price and total" do
      assert {:ok, %{price: price, total: total}} = fixtures(:buy, :book_single_aggregated_order)
                                                    |> Subject.quote(:sell, %{amount: 2, inverted?: true})
      assert {to_float(price), to_float(total)} == {0.001698888926641976147599469947, 0.003397777853283952295198939894}
    end

    test "with an amount which cannot be covered by multiple orders, returns an error" do
      assert {:error, :insufficient_orders} = fixtures(:buy, :book_multiple_orders)
                                              |> Subject.quote(:sell, %{amount: 1_000_000, inverted?: true})
    end

    test "with an amount which can be covered by multiple orders, returns the weighted price and total" do
      quote = fixtures(:buy, :book_multiple_orders) |> Subject.quote(:sell, %{amount: 200, inverted?: true})

      assert {:ok, %{price: price, total: total}} = quote
      assert %{price: 0.50475, total: 100.95} = %{price: to_float(price), total: to_float(total)}
    end
  end

  defp fixtures(:buy, fixture), do: fixtures(:asks, fixture)
  defp fixtures(:sell, fixture), do: fixtures(:bids, fixture)
  defp fixtures(key, :book_single_order), do: %{key => [{D.new(666), D.new(2), D.new(1)}]}
  defp fixtures(key, :book_single_aggregated_order) do
    %{
      key => [
       {D.new(777), D.new(2), D.new(1000)},
       {D.new(588.62), D.new(1.40981486), D.new(2)},
      ]
    }
  end

  defp fixtures(key, :book_multiple_orders) do
    %{
      key => [
       {D.new(300), D.new(0.1), D.new(4)},
       {D.new(200), D.new(0.7), D.new(2)},
       {D.new(0.1), D.new(100), D.new(1)},
      ]
    }
  end
end
