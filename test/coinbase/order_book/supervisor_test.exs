defmodule Coinbase.OrderBook.SupervisorTest do
  use ExUnit.Case

  alias Coinbase.OrderBook.Supervisor, as: Subject

  defmodule APIMock do
    def products, do: {:ok, [%{id: "ETH-EUR"}, %{id: "BTC-ETH"}]}
  end

  defmodule OrderBookMock do
    use GenServer

    def start_link(opts), do: GenServer.start_link(__MODULE__, opts)
    def init(_), do: {:ok, %{}}
    def child_spec(args), do: %{id: args.id, start: {__MODULE__, :start_link, [args]}}

    def book(_pid), do: {:error, nil}
  end

  setup do
    config = Application.get_env :coinbase, Subject
    test_config = config
                  |> put_in([:api], APIMock)
                  |> put_in([:orderbook], OrderBookMock)

    Application.put_env :coinbase, Subject, test_config

    on_exit fn -> Application.put_env :coinbase, Subject, config end

    :ok
  end

  describe "start_link/1" do
    test "registers a process name based on the given id" do
      {:ok, pid} = Subject.start_link(%{name: :test_sup})

      assert Process.info(pid, [:registered_name])[:registered_name] == :test_sup
    end
  end

  describe "initialization" do
    test "starts a child orderbook process per API product" do
      {:ok, pid} = Subject.start_link(%{name: name()})
      :timer.sleep 50 # Initialization is async, give it a few milliseconds

      assert ["BTC-ETH", "ETH-EUR"] = for {id, _pid, _, _} <- Supervisor.which_children(pid), do: id
    end
  end

  describe "find/2" do
    test "when the given id belongs to a child orderbook process without data, returns an error tuple" do
      {:ok, pid} = Subject.start_link(%{name: name()})
      :timer.sleep 50 # Initialization is async, give it a few milliseconds

      assert {:error, :book_empty} = Subject.find(pid, "BTC-ETH")
    end

    test "when the given id does not belong to a child orderbook, returns an error tuple with a message" do
      {:ok, pid} = Subject.start_link(%{name: name()})

      assert {:error, :book_not_found} = Subject.find(pid, "BTC-DOGE")
    end

    test "when the given id belongs to a child orderbook with data, returns a success tuple with the data" do
      defmodule OrderBookMockWithData do
        def child_spec(args), do: %{id: args.id, start: {__MODULE__, :start_link, [args]}}
        def book(_), do: {:ok, %{bids: [], asks: []}}
      end

      {:ok, pid} = Subject.start_link(%{name: name()})
      :timer.sleep 50 # Initialization is async, give it a few milliseconds

      config = Application.get_env(:coinbase, Subject) |> put_in([:orderbook], OrderBookMockWithData)
      Application.put_env :coinbase, Subject, config

      assert {:ok, %{asks: [], bids: []}} = Subject.find(pid, "BTC-ETH")
    end
  end

  defp name, do: :"#{inspect(make_ref())}"
end
