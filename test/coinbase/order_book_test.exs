defmodule Coinbase.OrderBookTest do
  use ExUnit.Case

  alias Coinbase.OrderBook, as: Subject

  defmodule APIMock do
    def book("ADA-DOGE", _opts), do: {:error, :invalid_id}
    def book(_id, _opts), do: {:ok, %{asks: [], bids: [], sequence: 2360345711}}
  end

  setup do
    config = Application.get_env :coinbase, Subject
    Application.put_env :coinbase, Subject, put_in(config[:api], APIMock)

    on_exit fn -> Application.put_env :coinbase, Subject, config end

    :ok
  end

  describe "start_link/1" do
    test "registers a process name based on the given id" do
      {:ok, pid} = Subject.start_link(%{id: "DOGE-ETH"})

      assert Process.info(pid, [:registered_name])[:registered_name] == :"order_book_DOGE-ETH"
    end

    test "starts with a nil book" do
      {:ok, pid} = Subject.start_link(%{id: "ADA-DOGE"})

      assert %{book: nil} = :sys.get_state(pid)
    end
  end

  describe "child_spec/1" do
    test "returns a valid child spec for the supervisor" do
      assert Subject.child_spec(%{id: "DOGE-ETH"}) == %{
        id: "DOGE-ETH",
        start: {Subject, :start_link, [%{id: "DOGE-ETH"}]}
      }
    end
  end

  describe "book/1" do
    test "when the book is nil, returns an error tuple" do
      {:ok, pid} = Subject.start_link(%{id: "ADA-DOGE"})

      assert {:error, nil} = Subject.book(pid)
    end

    test "when the book is not nil, returns a success tuple with the book" do
      {:ok, pid} = Subject.start_link(%{id: "DOGE-ETH"})
      {:ok, book} = APIMock.book("DOGE-ETH", %{})
      :sys.replace_state(pid, &(put_in(&1[:book], book)))

      assert {:ok, ^book} = Subject.book(pid)
    end
  end

  describe "refresh/1" do
    test "when the API request does not succeed, the state is not replaced" do
      {:ok, pid} = Subject.start_link(%{id: "ADA-DOGE"})

      Subject.refresh(pid)

      assert {:error, nil} = Subject.book(pid)
    end

    test "when the API request succeeds, the state is replaced" do
      {:ok, pid} = Subject.start_link(%{id: "ADA-DOGE"})

      :sys.replace_state(pid, &(put_in(&1[:id], "VALID-ID")))

      Subject.refresh(pid)

      {:ok, book} = APIMock.book("VALID-ID", %{})

      assert {:ok, ^book} = Subject.book(pid)
    end
  end
end
