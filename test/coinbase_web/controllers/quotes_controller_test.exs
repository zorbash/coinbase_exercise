defmodule CoinbaseWeb.QuotesControllerTest do
  use CoinbaseWeb.ConnCase
  alias CoinbaseWeb.QuotesController, as: Subject
  alias Decimal, as: D

  defmodule OrderBooksMock do
    def find("DOGE-ETH"), do: {:ok, %{asks: [], bids: []}}
    def find(_), do: {:error, :orderbook_not_found}
  end

  defmodule QuoteEngineMock do
    def quote(_book, :buy, %{amount: :invalid}) do
      {:error, :some_error}
    end

    def quote(_book, :buy, %{amount: amount, inverted?: true}) do
      price = 1 / 100
      {:ok, %{price: D.new(price), total: Decimal.new(price * amount)}}
    end

    def quote(_book, :buy, %{amount: amount}) do
      price = 100
      {:ok, %{price: D.new(price), total: Decimal.new(price * amount)}}
    end
  end


  setup do
    config = Application.get_env :coinbase, Subject
    test_config = config
                  |> put_in([:quote_engine], QuoteEngineMock)
                  |> put_in([:orderbooks], OrderBooksMock)

    Application.put_env :coinbase, Subject, test_config

    on_exit fn -> Application.put_env :coinbase, Subject, config end

    :ok
  end

  describe "POST /quote" do
    test "with missing params, responds with 400 and an error message", %{conn: conn} do
      conn = post conn, "/quote", %{quote_currency: "ETH"}
      %{"error" => message} = json_response(conn, :bad_request)

      assert message == "Missing required params: action, base_currency, amount"
    end

    test "with invalid action, responds with 400 and an error message", %{conn: conn} do
      conn = post conn, "/quote", %{action: "go fishing"}

      assert %{"error" => "invalid_action"} = json_response(conn, :bad_request)
    end

    test "with a unknown orderbook, returns a 422 and an error message", %{conn: conn} do
      conn = post conn, "/quote", %{action: "buy", base_currency: "ADA", quote_currency: "ETH", amount: 42}

      assert %{"error" => "orderbook_not_found"} = json_response(conn, :unprocessable_entity)
    end

    test "with a known orderbook, when the engine succeeds, returns 200 and the quote", %{conn: conn} do
      conn = post conn, "/quote", %{action: "buy", base_currency: "DOGE", quote_currency: "ETH", amount: 42}

      assert %{"currency" => "ETH", "price" => "100", "total" => "4200"} = json_response(conn, :ok)
    end

    test "with a known reverse orderbook, when the engine succeeds, returns 200 and the quote", %{conn: conn} do
      conn = post conn, "/quote", %{action: "buy", base_currency: "ETH", quote_currency: "DOGE", amount: 42}

      assert %{"currency" => "DOGE", "price" => "0.01", "total" => "0.42"} = json_response(conn, :ok)
    end

    test "with an unknown reverse orderbook, returns 422 and an error message", %{conn: conn} do
      conn = post conn, "/quote", %{action: "buy", base_currency: "DOGE", quote_currency: "ADA", amount: 42}

      assert %{"error" => "orderbook_not_found"} = json_response(conn, :unprocessable_entity)
    end
  end
end
