defmodule Coinbase.OrderBook.Supervisor do
  @moduledoc ~S"""
  Supervisor responsible for creating OrderBooks
  """

  use Confex, otp_app: :coinbase
  use Supervisor
  #alias Coinbase.OrderBook

  @doc """
  Starts the orderbook supervision tree
  """
  def start_link(opts) do
    Supervisor.start_link(__MODULE__, opts, name: opts[:name] || __MODULE__)
  end

  @doc false
  def init(_opts) do
    with sup <- self() do
      spawn fn -> initialize_order_books(sup) end
    end

    Supervisor.init([], strategy: :one_for_one)
  end

  @doc "Dynamically attaches a child process for each orderbook"
  def initialize_order_books(sup) do
    with {:ok, products} <- api().products do
      for product <- products, do: Supervisor.start_child(sup, {orderbook(), product})
    end
  end

  def find(id), do: find(__MODULE__, id)
  def find(sup, id) do
    with [pid] <- (for {^id, pid, _, _} <- Supervisor.which_children(sup), do: pid),
         {:ok, %{} = book} <- orderbook().book(pid) do
      {:ok, book}
    else
      {:error, nil} -> {:error, :book_empty}
      _ -> {:error, :book_not_found}
    end
  end

  defp api, do: config()[:api]
  defp orderbook, do: config()[:orderbook]
end
