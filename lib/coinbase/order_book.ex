defmodule Coinbase.OrderBook do
  @moduledoc ~S"""
  Module responsible to retrieve and keep an orderbook state.
  It refreshes from API on an interval basis and updates the state when the response is successful.

  Options

  `polling_interval`: Determines the interval in milliseconds to fetch the orderbook from
  the API and update the previous state.
  `level`: The level of detail of the ordebook to fetch from the API.
    See: https://docs.gdax.com/#get-product-order-book
  """

  use Confex, otp_app: :coinbase
  use GenServer

  def start_link(opts) do
    {:ok, _pid} = GenServer.start_link(__MODULE__, opts, name: :"order_book_#{opts[:id]}")
  end

  def child_spec(args) do
    %{id: args.id, start: {__MODULE__, :start_link, [args]}}
  end

  def book(pid), do: GenServer.call pid, :book
  def refresh(pid), do: GenServer.call pid, :refresh

  # Callbacks

  def init(state) do
    send self(), :refresh

    {:ok, state |> put_in([:book], nil)}
  end

  def handle_call(:book, _from, %{book: book} = state) when is_nil(book), do: {:reply, {:error, book}, state}
  def handle_call(:book, _from, %{book: book} = state), do: {:reply, {:ok, book}, state}

  def handle_call(:refresh, _from, state) do
    with new_state <- state |> fetch_book |> handle_refresh(state) do
      {:reply, new_state, new_state}
    end
  end

  def handle_info(:refresh, state) do
    Process.send_after self(), :refresh, config()[:polling_interval]

    {:noreply, state |> fetch_book |> handle_refresh(state)}
  end

  defp fetch_book(%{id: id}), do: api().book(id, %{level: config()[:level]})
  defp normalize_book(%{asks: asks, bids: bids} = book) do
    book
    |> put_in([:asks], normalize_book(asks))
    |> put_in([:bids], normalize_book(bids))
  end
  defp normalize_book(entries) do
    for [price, amount, n | _] <- entries, do: {Decimal.new(price), Decimal.new(amount), Decimal.new(n)}
  end

  defp handle_refresh({:ok, book}, state), do: %{state | book: (book |> normalize_book)}
  defp handle_refresh(_error, state), do: state
  defp api, do: config()[:api]
end
