defmodule Coinbase.API do
  @moduledoc ~S"""
  Module interfacing the gdax API of Coinbase.
  See: https://docs.gdax.com/#products
  """

  use Confex, otp_app: :coinbase
  use Tesla

  plug Tesla.Middleware.Tuples
  plug Tesla.Middleware.Logger
  plug Coinbase.API.Middleware.UserAgent
  plug Tesla.Middleware.JSON, engine_opts: [keys: :atoms]

  def products, do: get("#{base_url()}/products") |> handle_response
  def book(product, opts), do: get("#{base_url()}/products/#{product}/book", query: opts) |> handle_response

  defp handle_response({:ok, %{status: status, body: body}}) when status in 200..399, do: {:ok, body}
  defp handle_response({_, %{status: status}}), do: {:error, "failed with status: #{status}"}
  defp handle_response({_, %{message: message}}), do: {:error, "failed with message: #{message}"}

  defp base_url, do: config()[:base_url]
end
