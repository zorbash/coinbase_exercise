defmodule Coinbase.API.Middleware.UserAgent do
  @moduledoc ~S"""
  Middleware setting the User-Agent header for Coinbase API requests
  """

  @default_agent "zorbash exercise"

  @spec call(Tesla.Env.t(), list(), map()) :: Tesla.Env.t()
  def call(%{headers: headers} = env, next, opts) do
    %{env | headers: put_in(headers["User-Agent"], Access.get(opts, :user_agent, @default_agent))}
    |> Tesla.run(next)
  end
end
