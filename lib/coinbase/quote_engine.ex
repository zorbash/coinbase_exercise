defmodule Coinbase.QuoteEngine do
  @moduledoc ~S"""
  Module responsible to generate quotes to either buy or sell for the given
  orderbook data and user provided amount.

  Calculates the best price the user would be able to get for the request by executing
  trades on the given orderbook.

  As the given amount will rarely match a quantity in the order book exactly,
  orders are aggregated or parts of them are used to arrive at the exact
  quantity and the quote is the weighted average of those prices.
  """

  use Confex, otp_app: :coinbase
  import Decimal, only: [add: 2, mult: 2, sub: 2, cmp: 2]
  alias Decimal, as: D

  @type quote :: %{price: Decimal.t, total: Decimal.t}
  @spec quote(:buy | :sell, %{}, %{amount: number() | Decimal.t}) :: quote
  def quote(%{bids: bids}, :buy, %{inverted?: true} = opts), do: _quote(%{orders: bids |> Enum.reverse}, opts)
  def quote(%{asks: asks}, :sell, %{inverted?: true} = opts), do: _quote(%{orders: asks |> Enum.reverse}, opts)
  def quote(%{asks: asks}, :buy, opts), do: _quote(%{orders: asks}, opts)
  def quote(%{bids: bids}, :sell, opts), do: _quote(%{orders: bids}, opts)

  defp _quote(book, %{amount: amount} = opts) when is_bitstring(amount),
    do: _quote(book, %{opts | amount: Decimal.new(amount)})
  defp _quote(book, %{amount: amount} = opts) when is_number(amount),
    do: _quote(book, %{opts | amount: Decimal.new(amount)})
  defp _quote(_book, %{amount: %Decimal{coef: 0}}), do: {:error, :zero_amount}
  defp _quote(_book, %{amount: %Decimal{sign: -1}}), do: {:error, :negative_amount}
  defp _quote(%{orders: orders}, %{amount: %Decimal{} = amount} = opts) do
    case aggregate(orders, opts) do
      {:ok, %{weights: weights}} -> {:ok, format_quote(%{amount: amount, weights: weights})}
      error -> error
    end
  end

  defp format_quote(%{amount: amount, weights: weights}) do
    with price <- Enum.reduce(weights, D.new(0), fn {coef, price}, sum -> add(sum, mult(coef, price)) end),
         total <- mult(amount, price) do
      %{price: price, total: total}
    end
  end

  # Provides configuration about decimal operations to the given function
  defp with_config(fun) when is_function(fun) do
    D.with_context(%D.Context{precision: config()[:precision], rounding: config()[:rounding]}, fun)
  end

  defp aggregate(orders, %{amount: amount} = opts) do
    with_config(fn -> aggregate(orders, amount, Map.merge(opts, %{sum: Decimal.new(0), weights: []})) end)
  end
  defp aggregate(_orders, amount, %{sum: amount} = acc), do: {:ok, acc}
  defp aggregate([{price, size, num_orders} | rest], amount, %{inverted?: true, sum: sum, weights: weights} = opts) do
    remaining = sub(amount, sum)
    available = mult(size, num_orders) |> mult(price)
    inverse_price = D.div(D.new(1), price)

    case cmp(available, remaining) do
      result when result in [:gt, :eq] ->
        aggregate(rest, amount, %{opts | sum: amount, weights: [{D.div(remaining, amount), inverse_price} | weights]})
      _ ->
        aggregate(rest, amount, %{opts | sum: add(sum, available), weights: [{D.div(available, amount), inverse_price} | weights]})
    end
  end
  defp aggregate([{price, size, num_orders} | rest], amount, %{sum: sum, weights: weights} = opts) do
    remaining = sub(amount, sum)
    available = mult(size, num_orders)

    case cmp(available, remaining) do
      result when result in [:gt, :eq] ->
        aggregate(rest, amount, %{opts | sum: amount, weights: [{D.div(remaining, amount), price} | weights]})
      _ ->
        aggregate(rest, amount, %{opts | sum: add(sum, available), weights: [{D.div(available, amount), price} | weights]})
    end
  end
  defp aggregate([], _amount, _acc), do: {:error, :insufficient_orders}
end
