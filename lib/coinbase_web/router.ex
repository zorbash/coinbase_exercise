defmodule CoinbaseWeb.Router do
  use CoinbaseWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    # Ideally there should be authorization, rate-limiting, ..
  end

  scope "/", CoinbaseWeb do
    pipe_through :api # Use the default browser stack

    post "/quote", QuotesController, :quote
  end
end
