defmodule CoinbaseWeb.QuotesController do
  use CoinbaseWeb, :controller

  @required_params ~w[action base_currency quote_currency amount]
  @valid_actions ~w[buy sell]

  def quote(conn, %{"action" => act}) when act not in @valid_actions,
    do: conn |> put_status(:bad_request) |> json(%{error: :invalid_action})
  def quote(conn, %{"action" => _, "base_currency" => _, "quote_currency" => _, "amount" => _} = params),
    do: conn |> do_quote(params |> filter_params)

  def quote(conn, params) do
    conn
    |> put_status(:bad_request)
    |> json(%{error: "Missing required params: #{Enum.join(@required_params -- Map.keys(params), ", ")}"})
  end

  defp do_quote(conn, %{action: action, amount: amount, quote_currency: quote_currency} = params) do
    with {:ok, {book_opts, book}}  <- params |> fetch_book(),
         quote_opts <- Map.merge(book_opts, %{amount: amount}),
         {:ok, %{price: price, total: total}} <- book |> quote_engine().quote(action, quote_opts) do
      conn |> json(%{currency: quote_currency, price: price, total: total})
    else
      {:error, error} -> conn |> put_status(:unprocessable_entity) |> json(%{error: error})
    end
  end

  defp filter_params(%{"action" => action, "base_currency" => base, "quote_currency" => quote, "amount" => amount}) do
    %{action: action |> String.to_atom, base_currency: base, quote_currency: quote, amount: amount}
  end
  defp book_id(%{base_currency: base, quote_currency: quote}), do: "#{String.upcase(base)}-#{String.upcase(quote)}"

  defp fetch_book(%{base_currency: base, quote_currency: quote}) do
    case book_id(%{base_currency: base, quote_currency: quote}) |> orderbooks().find do
      {:ok, book} -> {:ok, {%{inverted?: false}, book}}
      {:error, _} ->
        case book_id(%{base_currency: quote, quote_currency: base}) |> orderbooks().find do
          {:ok, book} -> {:ok, {%{inverted?: true}, book}}
          error -> error
        end
    end
  end

  defp config(key), do: Application.get_env(:coinbase, __MODULE__)[key]
  defp quote_engine, do: config(:quote_engine)
  defp orderbooks, do: config(:orderbooks)
end
