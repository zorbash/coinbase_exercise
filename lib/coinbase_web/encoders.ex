defimpl Poison.Encoder, for: Decimal do
  def encode(number, options) do
    Poison.Encoder.BitString.encode(Decimal.to_string(number), options)
  end
end
