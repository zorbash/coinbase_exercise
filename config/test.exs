use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :coinbase, CoinbaseWeb.Endpoint,
  http: [port: 4001],
  server: false

config :coinbase, Coinbase.API,
  base_url: "https://api.gdax.local"

# Print only warnings and errors during test
config :logger, level: :warn
