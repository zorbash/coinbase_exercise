use Mix.Config

# Configures the endpoint
config :coinbase, CoinbaseWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "b3eZiCtQrghIjHOkfBQ3zxjzdu22UqFInYqdpvqPhULsFcv37tWqIbKqBaTqZHD5",
  render_errors: [view: CoinbaseWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Coinbase.PubSub,
           adapter: Phoenix.PubSub.PG2]

config :coinbase, Coinbase.API,
  base_url: {:system, "COINBASE_API_URL", "https://api.gdax.com"}

config :coinbase, Coinbase.OrderBook.Supervisor,
  api: Coinbase.API,
  orderbook: Coinbase.OrderBook

config :coinbase, Coinbase.OrderBook,
  polling_interval: {:system, :integer, "ORDER_BOOK_POLLING_INTERVAL", 20_000}, # milliseconds
  level: {:system, :integer, "ORDER_BOOK_LEVEL", 2},
  api: Coinbase.API

# See: https://hexdocs.pm/decimal/Decimal.Context.html#content
# for available configuration options
config :coinbase, Coinbase.QuoteEngine,
  precision: {:system, :integer, "QUOTE_ENGINE_PRECISION", 28},
  rounding: {:system, :atom, "QUOTE_ENGINE_ROUNDING", :half_even}

config :coinbase, CoinbaseWeb.QuotesController,
  quote_engine: Coinbase.QuoteEngine,
  orderbooks: Coinbase.OrderBook.Supervisor

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
